const cars = require('./cars')
const users = require('./users')
const roles = require('./roles')
const auth = require('./auth')

module.exports = {
    cars,
    users,
    roles,
    auth
}