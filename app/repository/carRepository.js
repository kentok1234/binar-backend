const {Cars} = require('../models')

module.exports = {
    create(data) {
        return Cars.create(data)
    },

    update(id, data) {
        return Cars.update(data, {
            where: {
                id,
            }
        })
    },

    delete(id) {
        return Cars.destroy({
            where: {
                id
            }
        })
    },

    find(id) {
        return Cars.findByPk(id)
    },

    findAll() {
        return Cars.findAll()
    },

    getTotalCars() {
        return Cars.count()
    }
}