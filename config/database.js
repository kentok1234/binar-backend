const {
    DB_USERNAME = null,
    DB_PASSWORD = "Pelajar92",
    DB_HOST = "localhost",
    DB_NAME = "cars"
} = process.env

module.exports = {
    "development": {
      "username": DB_USERNAME,
      "password": DB_PASSWORD,
      "database": `${DB_NAME}`,
      "host": DB_HOST,
      "dialect": "postgres"
    },
    "test": {
      "username": DB_USERNAME,
      "password": DB_PASSWORD,
      "database": `${DB_NAME}_test`,
      "host": DB_HOST,
      "dialect": "postgres"
    },
    "production": {
      "username": DB_USERNAME,
      "password": DB_PASSWORD,
      "database": `${DB_NAME}_production`,
      "host": DB_HOST,
      "dialect": "postgres"
    }
}  