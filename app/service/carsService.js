const carsRepository = require("../repository/carRepository")

module.exports = {
    create(request) {
        return carsRepository.create(request)
    },

    update(id, request) {
        return carsRepository.update(id, request)
    },

    delete(id) {
        return carsRepository.delete(id)
    },

    async list() {
        try {
            const cars = await carsRepository.findAll()
            const countCars = await carsRepository.getTotalCars()

            return {
                data: cars,
                count: countCars
            }
        }
        catch(error) {
            throw error
        }
    },

    get(id) {
        return carsRepository.find(id)
    }
}