const userService = require('../service/userService')
const bcrypt = require('bcryptjs')
const salt = 10

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, salt, (err, encryptedPassword) => {
            if (!!err) {
                reject(err)
                return
            }

            resolve(encryptedPassword)
        })
    })
}

async function getUsers(req, res) {
    try {
        const {data} = await userService.list()
        console.log(`GET [200] ${req.url}`)
        res.status(200).json({
            status: true,
            message: "Data User ditemukan",
            data: data,
        })
    }
    catch(err) {
        res.status(400).json({
            status: false,
            message: err.message
        })
    }
}

async function getUser(req, res) {
    try {
        const data = await userService.get(req.params.id)
        if (!data) {
            throw Error('Data User tidak ditemukan')
        }
        res.json({
            status: true,
            message: "Data User berhasil ditemukan",
            data: data,
        })
    }
    catch(err) {
        res.send(400, {
            success: false,
            message: err.message,
        })
    }
}

async function addUser(req, res) {
    try {
        if (!req.role.includes('superadmin')) {
            throw Error('Hanya superadmin yang bisa menambah')
        }

        const data = req.body
        
        const encryptedPassword = await encryptPassword(data.password)

        const User = await userService.create({
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: encryptedPassword,
            RoleId: data.RoleId
        })

        if (User) {
            console.log(`POST [200] ${req.url}`)
            res.json({
                success: true,
                message: "Data User berhasil dibuat",
                data: User,
            })
        }
    }
    catch(err) {
        console.log(`POST [400] ${req.url}`)
        res.status(400).json({
            success: false,
            message: err.message,
            data: null,
        })
    }
}

async function updateUser(req, res) {
    try {
        const data = req.body

        if (data.firstName == '' || data.firstName == null || data.firstName == undefined) {
            throw Error('Nama depan tidak boleh kosong')
        } 

        if (data.lastName == '' || data.lastName == null || data.lastName == undefined) {
            throw Error('Nama belakang tidak boleh kosong')
        } 

        if (data.email == '' || data.email == null || data.email == undefined) {
            throw Error('Email tidak boleh kosong')
        }

        if (!(data.password.length >= 6)) {
            throw Error('Password minimal 6 karakter')
        }

        const encryptedPassword = await encryptPassword(data.password)
        
        const user = await userService.update(req.params.id, {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: encryptedPassword,
            RoleId: data.RoleId
        })

        if (user) {
            res.json({
                success: true,
                message: 'Data user berhasil diperbarui',
            })
        }
    }
    catch(err) {
        console.log(err)
        res.send(400, {
            success: false,
            title: 'Data user gagal diperbarui',
            message: err.message
        })

    }
}

async function deleteUser(req, res) {
    try{
        const user = await userService.delete(req.params.id)

        if (user) {
            res.json({
                success: true,
                message: 'Data user berhasil dihapus'
            })
        }
    }
    catch(err) {
        res.json({
            success: false,
            title: 'Data user gagal dihapus',
            message: err.message
        })
    }
}

module.exports = {
    list: getUsers,
    get: getUser,
    create: addUser,
    update: updateUser,
    delete: deleteUser,

}