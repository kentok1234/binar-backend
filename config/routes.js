const express = require('express')
const multer = require('multer')
const path = require('path')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + file.originalname
        cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })

const upload = multer({storage: storage})
const controllers = require('../app/controllers')

const router = express.Router()

router.post('/login', controllers.auth.login)
router.post('/register', controllers.auth.authorize, controllers.users.create)
router.get('/whoami', controllers.auth.authorize, controllers.auth.whoAmI)

router.get('/cars', controllers.auth.authorize, controllers.cars.list)
router.get('/car/:id', controllers.auth.authorize, controllers.cars.get)
router.post('/cars', upload.single('car-image'), controllers.auth.authorize, controllers.cars.create)
router.put('/car/:id', upload.single('car-image'), controllers.auth.authorize, controllers.cars.update)
router.delete('/cars/:id', controllers.auth.authorize, controllers.cars.delete)

router.get('/users', controllers.users.list)
router.get('/users/:id', controllers.users.get)
router.put('/users/:id', controllers.users.update)
router.delete('/users/:id', controllers.users.delete)


router.get('/roles', controllers.roles.list)
router.post('/roles', controllers.roles.create)
router.get('/roles/:id', controllers.roles.get)
router.put('/roles/:id', controllers.roles.update)
router.delete('/roles/:id', controllers.roles.delete)

router.get('/image/:fieldname', (req, res) => {
    const {fieldname} = req.params
    const dirname = path.resolve()
    const fullPath = path.join(dirname, 'uploads/' + fieldname)
    res.sendFile(fullPath) 
})

module.exports = router