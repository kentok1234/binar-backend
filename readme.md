# Binar Back End

Repository yang hanya mengatur server dan dan API


## Installation

Pertama install terlebih dahulu package yang dibutuhkan

```bash
  npm install
```

Project ini menggunakan nodemon untuk menjalankannya jadi harap install nodemon sebelum menjalankan aplikasi

Setelah itu ketikkan perintah berikut :
```bash
    npm start
```

## Informasi Login
  email: admin@email.com
  password: admin123

## List endpoint 
- POST /login
  ```
    {
      "email": string,
      "password": string
    }
  ```
- POST /register
  ```
    {
      "firstName": string,
      "lastName": string,
      "email": string,
      "password": string,
      "RoleId": integer,
    }
  ```
- GET /whoami
  return current user
- GET /cars
  get list of cars
- GET /car/:id
  get a car
- POST /cars
  create a new car
  ```
    {
      "name": string,
      "rent": integer,
      "size": string,
      "image": string (path),
    }
  ```
- PUT /car/:id
  update a specific car
  ```
    {
      "name": string,
      "rent": integer,
      "size": string,
      "image": string (path),
    }
  ```
- DELETE /cars/:id
  delete a specific car
- GET /users
  get list of users
- GET /users/:id
  get a specific user
- PUT /users/:id
  update a specific user
   ```
    {
      "firstName": string,
      "lastName": string,
      "email": string,
      "password": string,
      "RoleId": integer,
    }
  ```
- DELETE /users/:id
  delete user by id
- GET /roles
  get list of roles
- POST /roles
  create a new roles
  ```
    {
      "name": string,
      "description": string,
    }
  ```
- GET /roles/:id
  get a role
- PUT /roles/:id
  Update a role
   ```
    {
      "name": string,
      "description": string,
    }
  ```
- DELETE /roles/:id
  delete a role