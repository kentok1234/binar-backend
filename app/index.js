const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const cookie = require('cookie-parser')
const cors = require('cors')
const router = require('../config/routes')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(cors({
    origin: 'http://localhost:8080'
}))
app.use(cookie())

app.use(router)

module.exports = app