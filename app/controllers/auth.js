const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const userService = require('../service/userService')
const roleService = require('../service/roleService')

async function authorize(req, res, next) {
    try {
        const bearToken = req.headers.authorization
        const token = bearToken.split("Bearer ")[1]
        const payload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "secret")

        req.user = await userService.get(payload.id.toString())

        const role = await roleService.get(req.user.RoleId)
        req.role = role.name

        next()
    }
    catch(err) {
        console.log(err)
        res.status(401).json({
            message: "Unauthorized",
        })
    }
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(
            password,
            encryptedPassword,
            (err, isPasswordCorrect) => {
                if (!!err) {
                    reject(err)
                    return
                }

                resolve(isPasswordCorrect)
            }
        )
    })
}

function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "secret")
}

function whoAmI(req, res) {
    res.status(200).json(req.user)
}

async function login(req, res) {
    try {
        const email = req.body.email.toLowerCase()
        const password = req.body.password
    
        const user = await userService.get(email)
    
        if(!user) {
            throw Error('Email tidak ditemukan')
        }

        const isPasswordCorrect = await checkPassword(user.password, password)

        if(!isPasswordCorrect) {
            throw Error('Password salah!')
        }

        const token = createToken({
            id: user.id,
            email: user.email
        })

        res.json({
            status: true,
            message: 'User berhasil login',
            data: user,
            token: token
        })

    }
    catch(err) {
        res.status(400).json({
            status: false,
            message: err.message
        })
    }
}

module.exports = {
    authorize: authorize,
    login: login,
    whoAmI
}