const carsService = require('../service/carsService')

async function getCars(req, res) {
    try {
        if (!['superadmin', 'admin', 'member'].includes(req.role)) {
            throw Error('Hanya superadmin dan admin yang bisa melihat')
        }

        const {data, count} = await carsService.list()
        console.log(`GET [200] ${req.url}`)
        res.status(200).json({
            status: true,
            message: "Data mobil ditemukan",
            data: data,
            meta: {total: count}
        })
    }
    catch(err) {
        res.status(400).json({
            status: false,
            message: err.message
        })
    }
}

async function getCar(req, res) {
    try {
        if (!['superadmin', 'admin', 'member'].includes(req.role)) {
            throw Error('Hanya superadmin dan admin yang bisa melihat')
        }

        const data = await carsService.get(req.params.id)
        res.json({
            status: true,
            message: "Data mobil berhasil ditemukan",
            data: data,
        })
    }
    catch(err) {
        res.send(400, {
            success: false,
            message: err.message,
        })
    }
}

async function addCar(req, res) {
    try {
        if (!['superadmin', 'admin'].includes(req.role)) {
            throw Error('Hanya superadmin dan admin yang bisa menambah')
        }

        const data = req.body
        if (data.name == '' || data.name == null || data.name == undefined) {
            throw Error('Nama mobil harus diisi')
        }
        if (parseFloat(data.rent) < 0) {
            throw Error('Harga sewa tidak boleh negatif')
        } else if (data.rent == '' || data.rent == null || data.rent == undefined) {
            throw Error('Harga sewa harus diisi')
        }

        const car = await carsService.create({
            name: data.name,
            rent: parseInt(data.rent, 10),
            size: data.size,
            image: req.file.filename
        })
        if (car) {
            console.log(`POST [200] ${req.url}`)
            res.json({
                success: true,
                message: "Data mobil berhasil dibuat",
                data: car,
            })
        }
    }
    catch(err) {
        console.log(`POST [400] ${req.url}`)
        res.status(400).json({
            success: false,
            message: err.message,
            data: null,
        })
    }
}

function updateCar(req, res) {
    try {
        if (!['superadmin', 'admin'].includes(req.role)) {
            throw Error('Hanya superadmin dan admin yang bisa memperbarui')
        }

        const data = req.body
        console.log(req.file)

        if (data.name == '' || data.name == null || data.name == undefined) {
            throw Error('Nama tidak boleh kosong')
        }

        const car = carsService.update(req.params.id, {
            name: data.name,
            rent: parseInt(data.rent, 10),
            size: data.size,
            image: req.file.filename,
        })

        if (car) {
            res.json({
                success: true,
                message: 'Data mobil berhasil diperbarui',
                data: car,
            })
        }
    }
    catch(err) {
        res.send(400, {
            success: false,
            title: 'Data mobil gagal diperbarui',
            message: err.message
        })

    }
}

async function deleteCar(req, res) {
    try{
        if (!['superadmin', 'admin'].includes(req.role)) {
            throw Error('Hanya superadmin dan admin yang bisa menghapus')
        }

        const car = await carsService.delete(req.params.id)

        if (car) {
            res.send(200, {
                success: true,
                message: 'Data mobil berhasil dihapus'
            })
        }
    }
    catch(err) {
        res.send(400, {
            success: false,
            title: 'Data mobil gagal dihapus',
            message: err.message
        })
    }
}

module.exports = {
    list: getCars,
    get: getCar,
    create: addCar,
    update: updateCar,
    delete: deleteCar,
}