const {User} = require('../models')

module.exports = {
    create(data) {
        return User.create(data)
    },
    update(id, data) {
        return User.update(data, {
            where: {
                id
            }
        })
    },
    delete(id) {
        return User.destroy({
            where: {
                id
            }
        })
    },

    find(id) {
        if (!id.includes('@')) return User.findByPk(id)
        return User.findOne({
            where: {
                email: id
            }
        })
    },

    findAll() {
        return User.findAll()
    },
}