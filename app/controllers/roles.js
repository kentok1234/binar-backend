const roleService = require('../service/roleService')

async function getRoles(req, res) {
    try {
        const {data} = await roleService.list()
        console.log(`GET [200] ${req.url}`)
        res.status(200).json({
            status: true,
            message: "Data Role ditemukan",
            data: data,
        })
    }
    catch(err) {
        res.status(400).json({
            status: false,
            message: err.message
        })
    }
}

async function getRole(req, res) {
    try {
        const data = await roleService.get(req.params.id)
        if (!data) {
            throw Error('Data Roles tidak ditemukan')
        }
        res.json({
            status: true,
            message: "Data Roles berhasil ditemukan",
            data: data,
        })
    }
    catch(err) {
        res.send(400, {
            success: false,
            message: err.message,
        })
    }
}

async function addRole(req, res) {
    try {
        const data = req.body

        if (!(data.name && data.description)) throw Error("Data harus diisi")

        const roles = await roleService.create({
            name: data.name,
            description: data.description
        })
        if (roles) {
            console.log(`POST [200] ${req.url}`)
            res.json({
                success: true,
                message: "Data role berhasil dibuat",
                data: roles,
            })
        }
    }
    catch(err) {
        console.log(`POST [400] ${req.url}`)
        res.status(400).json({
            success: false,
            message: err.message,
            data: null,
        })
    }
}

function updateRole(req, res) {
    try {
        const data = req.body

        if (data.name == '' || data.name == null || data.name == undefined) {
            throw Error('Nama tidak boleh kosong')
        }

        const role = roleService.update(req.params.id, {
            name: data.name,
            description: data.description
        })

        if(role) res.json({
            success: true,
            message: 'Data role berhasil diperbarui',
        })

    }
    catch(err) {
        res.send(400, {
            success: false,
            title: 'Data role gagal diperbarui',
            message: err.message
        })

    }
}

function deleteRole(req, res) {
    try{
        const role = roleService.delete(req.params.id)

        if (role) {
            res.send('Data role sudah dihapus')

        }
    }
    catch(err) {
        res.send('Data role gagal terhapus')
    }
}

module.exports = {
    list: getRoles,
    get: getRole,
    create: addRole,
    update: updateRole,
    delete: deleteRole,
}