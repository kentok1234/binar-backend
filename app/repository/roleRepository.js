const {Role} = require('../models')

module.exports = {
    create(data) {
        return Role.create(data)
    },
    update(id, data) {
        return Role.update(data, {
            where: {
                id
            }
        })
    },
    delete(id) {
        return Role.destroy(id)
    },

    find(id) {
        return Role.findByPk(id)
    },

    findAll() {
        return Role.findAll()
    },
}