const userRepository = require("../repository/userRepository")

module.exports = {
    create(request) {
        return userRepository.create(request)
    },

    update(id, request) {
        return userRepository.update(id, request)
    },

    delete(id) {
        return userRepository.delete(id)
    },

    async list() {
        try {
            const cars = await userRepository.findAll()

            return {
                data: cars,
            }
        }
        catch(error) {
            throw error
        }
    },

    get(id) {
        return userRepository.find(id)
    }
}