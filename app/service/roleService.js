const roleRepository = require("../repository/roleRepository")

module.exports = {
    create(request) {
        return roleRepository.create(request)
    },

    update(id, request) {
        return roleRepository.update(id, request)
    },

    delete(id) {
        return roleRepository.delete(id)
    },

    async list() {
        try {
            const cars = await roleRepository.findAll()

            return {
                data: cars,
            }
        }
        catch(error) {
            throw error
        }
    },

    get(id) {
        return roleRepository.find(id)
    }
}